<?php
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_DATABASE", "tagme");


class Database
{

	private $conn;
	function __construct()
	{
		
	}

	function __destruct()
	{
		$this->close();
	}

	//method for closing dataconnetion
	public function close()
	{
			//mysqli_close($this->conn);
			unset($this->conn);
	}

	//method  for mysql_connection
	public function connect()
	{
		$this->conn=mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_DATABASE) or die("Connection error");
		//$this->conn=mysqli_connect("localhost","root","","flirtapp") or die("Connection error");
	}

	//method for login
	public function login($email,$password)
	{
		$password=md5($password);//password hashing
		$query="SELECT * FROM users WHERE email='$email' AND pass='$password'";
			$result = mysqli_query($this->conn,$query);
			if(mysqli_num_rows($result)==1)
			{
				$row=mysqli_fetch_array($result);
				$userID=$row["userID"];
				$_SESSION["ID"] = $userID;
				$array["userID"] = $userID;
				$array["UserName"] = $row["username"];
				$array["FirstName"] = $row["first_name"];
				$array["LastName"] = $row["last_name"];
				$array["Email"] = $row["email"];
				$array["RegistrationDate"] = $row["registration_date"];
				$array["FirstTime"] = $row["first_time"];
				$array["success"] = true;
				$array["active"] = $this->isActive($userID);
				return $array;
			}
			else
			{
				$array["userID"] = null;
				$array["success"] = false;
				$array["active"] = null;
				return $array;
			}
	}


	/*check if user has activated account*/
	public function isActive($userID)
	{
		$query="SELECT * FROM `users` WHERE `userID`='$userID' AND `activated`=1";
		$result =mysqli_query($this->conn,$query) or die("IsActiv error");
		
		if($result->num_rows==1)
		{
			
			return true;
		}
		return false;
	}

	/*
		REGISTRATION
	*/

	public function register($username,$fName,$lName,$email,$pass)
	{
		$usernameValidQuery = "SELECT * FROM `users` WHERE  `username`='$username'";
		$emailValidQuery = "SELECT * FROM `users` WHERE `email`='$email'";

		//execute
		$usernameValidQuery = mysqli_query($this->conn,$usernameValidQuery);
		$emailValidQuery = mysqli_query($this->conn,$emailValidQuery);

		if(mysqli_num_rows($usernameValidQuery) == 1){
				$resArray["succes"] = false;
				$resArray["message"] = "Username exists!";
				return $resArray;
			}
		else if(mysqli_num_rows($emailValidQuery) == 1){
				$resArray["succes"] = false;
				$resArray["message"] = "Email exists!";
				return $resArray;
			}

		else
		{
			$password=md5($pass);//password hashing
			$date = date_create();
			$regDate = date_timestamp_get($date);
			$rnum = rand(10,15000);
			$act_link = md5($rnum);

			$insertQuery = "INSERT INTO `users`
			( `username`, `first_name`, `last_name`, `email`, `registration_date`, `activated`, `activation_link`, `first_time`, `pass`) 
			VALUES ('$username','$fName','$lName','$email',$regDate,0,'$act_link',1,'$password')";

			//execute quert
			$result = mysqli_query($this->conn,$insertQuery);
			if($result)
			{
				$resArray["succes"] = true;
				$resArray["message"] = "Profile created!";
				$resArray["act_link"] = $act_link;
				return $resArray;
			}

			else
			{
				$resArray["succes"] = false;
				$resArray["message"] = "Mysql error!";
				return $resArray;
			}
		}

	}
}
?>