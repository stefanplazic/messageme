package data;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by stefan on 8/2/2016.
 */
public class PrefClass {

    private Context context = null;


    public PrefClass(Context context) {
        this.context = context;
    }

    public PrefClass(){}

    public boolean isLogged()
    {
        SharedPreferences pref = this.context.getSharedPreferences("appData" ,Context.MODE_PRIVATE);
        boolean userLogged = pref.getBoolean("userLogged",false);

        return  userLogged;
    }

    public  void writeData(boolean data)
    {
        SharedPreferences pref = this.context.getSharedPreferences("appData" ,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("userLogged",data);
        editor.commit();
    }

    /*
    * getters and
    * setterr
    * */

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
