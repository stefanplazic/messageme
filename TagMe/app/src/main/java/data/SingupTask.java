package data;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.plazic.stefan.tagme.SingupActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by stefan on 8/7/2016.
 */
public class SingupTask  extends AsyncTask<String,Void,JSONObject>{

    private String username;
    private String email;
    private String pass;

    private SingupActivity context;
    private  JSONObject resultJson;

    public SingupTask() {
    }

    public SingupTask(String username, String email, String pass, SingupActivity context) {
        this.username = username;
        this.email = email;
        this.pass = pass;
        this.context = context;

    }

    @Override
    protected JSONObject doInBackground(String... params) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        String resultString = null;

        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            Uri.Builder uriBuilder = new Uri.Builder();

            uriBuilder.encodedPath("http://192.168.0.52:81/tagme/register.php");
            uriBuilder.appendQueryParameter("mode", "POST");
            URL url = new URL(uriBuilder.build().toString());

            // Create the request to OpenWeatherMap, and open the connection



            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");

            //URLConnection urlCon = url.openConnection();

            String data = "email="+this.email+"&password="+this.pass+"&username="+this.username;

            urlConnection.setDoOutput(true);

            OutputStreamWriter ow = new OutputStreamWriter(urlConnection.getOutputStream());
            ow.write(data);
            ow.flush();


            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                //return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            resultString = buffer.toString();
            Log.v("Result", "Forecast JSON string: " + resultString);
            return  new JSONObject(resultString);


        } catch (IOException e) {
            // Log.e("PlaceholderFragment", "Error ", e);
            Log.e("Error", " Exection type: " + e.getMessage().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("final", "Error closing stream", e);
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        setResultJson(jsonObject);//set json field
        parseUser();

    }

    /*CONVERT JSON to USER class*/
    public  void parseUser()
    {
        this.context.turnOfProgress();//hide dialog
        try {
            String success = this.resultJson.getString("succes");
            if(success.equals("true")){
                PrefClass prefClass = new PrefClass(this.context);
                prefClass.writeData(true);
                //this.context.finish();
                Toast.makeText(this.context,"Register sucess",Toast.LENGTH_LONG).show();

            }
            else
            {
                //show error message
                String message = this.resultJson.getString("message");
                Toast.makeText(this.context,message,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    /*GETTERS AND SETTERS*/

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public SingupActivity getContext() {
        return context;
    }

    public void setContext(SingupActivity context) {
        this.context = context;
    }

    public JSONObject getResultJson() {
        return resultJson;
    }

    public void setResultJson(JSONObject resultJson) {
        this.resultJson = resultJson;
    }
}
