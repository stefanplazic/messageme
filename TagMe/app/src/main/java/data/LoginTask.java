package data;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.plazic.stefan.tagme.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by stefan on 8/6/2016.
 */
public class LoginTask extends AsyncTask<String,Void,JSONObject>{

    private  String email;
    private String pass;
    private LoginActivity context;

    private  JSONObject resultJson;

    public LoginTask(String email, String pass, LoginActivity context) {
        this.email = email;
        this.pass = pass;
        this.context = context;

    }

    public  LoginTask(){}

    @Override
    protected JSONObject doInBackground(String... params) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;



        String resultString = null;


        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            Uri.Builder uriBuilder = new Uri.Builder();

            uriBuilder.encodedPath("http://192.168.0.52:81/tagme/login.php");
            uriBuilder.appendQueryParameter("mode", "POST");
            URL url = new URL(uriBuilder.build().toString());

            // Create the request to OpenWeatherMap, and open the connection



            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");

            //URLConnection urlCon = url.openConnection();

            String data = "email="+this.email+"&password="+this.pass;

            urlConnection.setDoOutput(true);

            OutputStreamWriter ow = new OutputStreamWriter(urlConnection.getOutputStream());
            ow.write(data);
            ow.flush();


            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                //return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            resultString = buffer.toString();
            Log.v("Result", "Forecast JSON string: " + resultString);
            return  new JSONObject(resultString);


        } catch (IOException e) {
            // Log.e("PlaceholderFragment", "Error ", e);
            Log.e("Error", " Exection type: " + e.getMessage().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("final", "Error closing stream", e);
                }
            }
        }
        return null;

    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {

        setResultJson(jsonObject);
        handleUser(); // call method for handlig user
    }

    /*GETTERS AND SETTERS*/

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public JSONObject getResultJson() {
        return resultJson;
    }

    public void setResultJson(JSONObject resultJson) {
        this.resultJson = resultJson;
    }

    public LoginActivity getContext() {
        return context;
    }

    public void setContext(LoginActivity context) {
        this.context = context;
    }

    public  static  User parseUser(JSONObject jsonObject){

        try {
            String success = jsonObject.getString("success");
            //
            //check the success
            if(success.equals("true"))
            {
                String userName = jsonObject.optString("UserName", "");
                String Email = jsonObject.optString("Email", "");
                String FirstName = jsonObject.optString("FirstName", "");
                String LastName = jsonObject.optString("LastName", "");
                User profile = new User();
                //set userdata
                profile.setUsername(userName);
                profile.setEmail(Email);
                profile.setFirstName(FirstName);
                profile.setLastName(LastName);


                return profile;
            }
            //if it is not good
            else
            {
                return  null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  null;
    }


    /*handle the result and go to main activity*/
    public void handleUser(){
        User user = parseUser(this.resultJson);
        this.context.turnofProgress();
        this.context.loginResponse(user);
    }
}
