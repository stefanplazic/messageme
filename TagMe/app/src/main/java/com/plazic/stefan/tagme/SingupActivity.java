package com.plazic.stefan.tagme;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import data.SingupTask;

public class SingupActivity extends AppCompatActivity {

    /*static codes for intent operations*/
    private  final  static  int GALLERY_CODE = 1;
    private  final  static  int TAKE_IMAGE_CODE = 2;
    private final  static int PIC_CROP = 3;

    private Uri picUri;

    @InjectView(R.id.input_password) EditText passField;
    @InjectView(R.id.input_username) EditText usernameField;
    @InjectView(R.id.input_email) EditText emailField;
    @InjectView(R.id.btn_signup) Button singupBtn;
    @InjectView(R.id.avatar_singup) ImageView avatarImage;

    //make progressDialog
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.progressDialog  = new ProgressDialog(SingupActivity.this);
        //inject this
        ButterKnife.inject(this);
        singupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singup();
            }
        });

        setAvatarIf();
        avatarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateAvatar();
            }
        });


    }

    /*methods for setting and turning off the progress dialog*/
    public  void  turnOnProgress()
    {
        progressDialog.setMessage(getString(R.string.regDialogMesaage));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void turnOfProgress()
    {
        progressDialog.hide();
    }

    private void setAvatarIf() {
        String path = Environment.getExternalStorageDirectory().toString();
        File fi = new File(path,"avatar.png");

        if(fi.exists()){

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap photo = BitmapFactory.decodeFile(fi.getAbsolutePath());
            this.avatarImage.setImageBitmap(photo);
        }
    }


    /*
    * select image from galery or take it from camera
    * * */
    private void updateAvatar() {
        final CharSequence[] options = { "Take Photo", "Gallery" };
        AlertDialog.Builder builder = new AlertDialog.Builder(SingupActivity.this);
        builder.setItems(options,new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {

                //check witch option is selected
                if(options[which].equals("Take Photo"))
                {
                    Intent takeIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takeIntent,TAKE_IMAGE_CODE);
                    //Toast.makeText(getApplicationContext(), f, Toast.LENGTH_SHORT).show();

                }
                else
                {
                    Intent mediaIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(mediaIntent,GALLERY_CODE);
                }
            }
        });

        //show the dialog
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if is image capture
        if(requestCode == TAKE_IMAGE_CODE && resultCode == RESULT_OK){

            Bitmap photo = (Bitmap) data.getExtras().get("data");

            this.avatarImage.setImageBitmap(photo);

            //save the image
            String path = Environment.getExternalStorageDirectory().toString();
            FileOutputStream fous = null;
            File fi = new File(path,"avatar.png");
            try {
                fous = new FileOutputStream(fi);

                //save bitmap
                photo.compress(Bitmap.CompressFormat.PNG,100,fous);
                fous.flush();
                fous.close();
                MediaStore.Images.Media.insertImage(getContentResolver(),fi.getAbsolutePath() ,fi.getName(),fi.getName());

            } catch (FileNotFoundException e) {
                //Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                //Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
            }
        }

        //if gallery
        else if (requestCode == GALLERY_CODE && resultCode == RESULT_OK){


        }
        //crop picture
        else if (requestCode == PIC_CROP && resultCode == RESULT_OK){

            Bundle extras = data.getExtras();
            Bitmap thePic = extras.getParcelable("data");
            this.avatarImage.setImageBitmap(thePic);
        }
    }

    /*
        * perform singing up activity
        * */
    private void singup()
    {
        String username = usernameField.getText().toString();
        String pass = passField.getText().toString();
        String email = emailField.getText().toString();

        if(validate(username,pass,email))
        {
            //Toast.makeText(getApplicationContext(), "Singing up", Toast.LENGTH_SHORT).show();
            turnOnProgress();//sow progressDialog
            SingupTask st = new SingupTask(username,email,pass,this);//create asynctask for user registration
            st.execute();
        }
    }

    /*
    * check if all data is in correct format
    * */
    private boolean validate(String username, String pass, String email) {


        boolean valid = true;

        //validate username
        if(username.length() < 3 )
        {
            usernameField.setError("3 or more chars!");
            valid=false;
        }
        else
        {
            usernameField.setError(null);
            valid=true;
        }

        //validate email
        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches() )
        {
            emailField.setError("should be like: examle@gmail.com");
            valid=false;
        }
        else
        {
            emailField.setError(null);
            valid=true;
        }


        //validate password
        if(pass.length() < 3 )
        {
            passField.setError("3 or more chars!");
            valid=false;
        }
        else
        {
            passField.setError(null);
            valid=true;
        }

        return  valid;
    }

    /*crop the image*/
    private void cropAvatar()
    {
        try{

            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        catch (ActivityNotFoundException ex){

        }
    }

}
