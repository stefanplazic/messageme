package com.plazic.stefan.tagme;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import data.PrefClass;

public class MainActivity extends AppCompatActivity {

    private  Intent searchIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.friendList);

        checkUser();

        //intents
        searchIntent = getIntent();
        if(searchIntent.getAction().equals(Intent.ACTION_SEARCH)) {
            searchMe();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchView sView = (SearchView) menu.findItem(R.id.menu_searchable).getActionView();
        SearchManager sMan = (SearchManager) getSystemService(SEARCH_SERVICE);

        sView.setSearchableInfo(sMan.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){

            case R.id.action_logout:
                logoutYesNo();
                break;
            case R.id.action_settings:

                break;
        }



        return super.onOptionsItemSelected(item);
    }

    /*shows alert dialog with yes/no buttons */
    private void logoutYesNo()
    {
        final PrefClass prefClass = new PrefClass(this);
        final  Intent intent = new Intent(this, LoginActivity.class);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.loutdialogTitle);
        builder.setMessage(R.string.loutDialogMessage);
        //set positiv button OK
        builder.setPositiveButton(R.string.loutDialogOK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                prefClass.writeData(false);//unlog user from pref settings
                //go to loginactivity

                startActivity(intent);
            }
        });
        builder.setNegativeButton(R.string.loutDialogCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();


    }

    /*
    * method for working with search intent
    *
    * */
    private  void searchMe(){
        String searchQuery = searchIntent.getStringExtra(SearchManager.QUERY);
        Toast.makeText(MainActivity.this,searchQuery, Toast.LENGTH_SHORT).show();

    }

    /*
    * check if user is logged | if not -> go to login activity
    * */

    private  void checkUser()
    {
        PrefClass pefClass = new PrefClass(getApplicationContext());

        boolean isLogged = pefClass.isLogged();


        if(!isLogged)
        {
            //go to login activity
            Intent logIntent = new Intent(this,LoginActivity.class);
            startActivity(logIntent);
        }


    }
}
