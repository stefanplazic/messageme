package com.plazic.stefan.tagme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import data.LoginTask;
import data.PrefClass;
import data.User;

public class LoginActivity extends AppCompatActivity {

    /*STATIC CODES*/
    private  final  static  int REQUEST_SINGUP = 1;

    private EditText emailField;
    private EditText passField;
    private Button loginBtn;
    private TextView singupLink;
    private ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //init elems
        initElems();
        this.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        //singuplink click
        this.singupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go to singupActivity for registration
                Intent singupIntent = new Intent(getApplicationContext(),SingupActivity.class);
                startActivityForResult(singupIntent,REQUEST_SINGUP);
            }
        });

    }
    /*stop users from going to mainactivity, even if they press back button*/
    @Override
    public void onBackPressed() {

    }

    /*
    *  call when login button pressed
    */
    private void login()
    {

        //get user text
        String userPass = this.passField.getText().toString();
        String userEmail = this.emailField.getText().toString();

        if(validate_data(userPass,userEmail))
        {

            //Toast.makeText(getApplicationContext(),"Connecting to server", Toast.LENGTH_SHORT).show();

            progressDialog.setIndeterminate(false);


            LoginTask lt = new LoginTask(userEmail,userPass,this);
            turnonProgress();
            lt.execute();



        }

    }

    public void turnonProgress()
    {
        progressDialog.setMessage(getString(R.string.loginProgressDialog));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void turnofProgress()
    {
        progressDialog.hide();

    }

    /*finish the activity*/
    public  void loginResponse(User user)
    {
        if (user!=null)
        {
            PrefClass pefClass = new PrefClass(getApplicationContext());
            pefClass.writeData(true); //set user to be logged
            finish();

        }
        else
            Toast.makeText(getApplicationContext(),"Wrong login combination",Toast.LENGTH_LONG).show();
    }
    /*
    * for validating email and pass data
    * */
    private boolean validate_data(String pass, String email)
    {
        boolean validate = true;
        if(email.isEmpty() || ! android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            this.emailField.setError("Should be like : examle@gmail.com");
            validate = false;
        }
        else
        {
            this.emailField.setError(null);
            validate = true;
        }

        if(pass.length()<3)
        {
            this.passField.setError("Minumum 3 chars!");
            validate = false;
        }
        else
        {
            this.passField.setError(null);
            validate = true;
        }
        return validate;
    }

    private void initElems()
    {
        this.emailField = (EditText) findViewById(R.id.input_email);
        this.loginBtn = (Button) findViewById(R.id.btn_login);
        this.passField = (EditText) findViewById(R.id.input_password);
        this.singupLink = (TextView) findViewById(R.id.link_signup);
        this.progressDialog = new ProgressDialog(LoginActivity.this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}
